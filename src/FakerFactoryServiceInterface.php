<?php

namespace Drupal\migrate_faker;

/**
 * Interface for Faker Factory Service.
 */
interface FakerFactoryServiceInterface {

  /**
   * Returns an instance of a Faker generator.
   *
   * @param string $locale
   *   The locale to use when configuring the generator.
   */
  public function create(string $locale);

}
