This modules allows to use the Faker package to create fake data when running
migrations.

You can either use the default faker instance, or create a custom service to
instantiate a custom faker instance.

**Mode of use: Simple approach**

In this example, the [Faker::text](https://fakerphp.github.io/formatters/text-and-paragraphs/#text)
will be called to generate a fake English based text.

```
process:
  field_text/value:
    - plugin: faker
      generator: text
```


```
process:
  field_number:
    - plugin: faker
      generator: randomDigit
```


```
process:
  field_number:
    - plugin: faker
      generator: numberBetween  # Use args to specify argument to the generator.
      args:
        - 20
        - 30
```

Some generators produce an array of values, you can join them using the contact plugin.

```
process:
  field_text/value:
    - plugin: faker
      generator: paragraphs
    - plugin: concat
      delimiter: "\n\n"
```


**Mode of use: Custom service**

```
process:
  field_text/value:
    - plugin: faker
      generator: text
      #This service will be in charge or creating the faker instance
      faker_factory_service: mymodule.faker_factory_service
      # The locale to use when creating the faker instance
      faker_locale: _locale
```

```
<?php

namespace Drupal\mymodule;

use Drupal\migrate_faker\FakerFactoryServiceInterface;
use Faker\Factory;

/**
 * A Faker Factory service to create English or Arabic strings.
 */
class FakerFactoryService implements FakerFactoryServiceInterface {

  /**
   * {@inheritdoc}
   */
  public function create(string $locale) {
    $faker = Factory::create($locale);
    if ($locale === 'ar_SA') {
      // TextProviderEn is a custom class that extends use Faker\Provider\ar_SA\Text
      // to define custom Arabic content;
      $faker->addProvider(new TextProviderAr($faker));
      return $faker;
    }

    // TextProviderEn is a custom class that extends use Faker\Provider\en_US\Text
    // to define custom English content;
    $faker->addProvider(new TextProviderEn($faker));
    return $faker;
  }

}
```

